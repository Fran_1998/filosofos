#include<iostream>
#include<mutex>
#include<thread>
#include<ctime>
#include<chrono>

using namespace std;

//b) Se pueden usar una cantidad cualquiera de filósofos definida por el usuario.
const int num_filosofos=5;//nro de Filosofos

//a) Un filósofo es un Thread que pasa su vida pensando o comiendo.
thread filosofos[num_filosofos];//threads de los filosofos

mutex mtx[num_filosofos];//cubiertos
mutex cout_mutex;
int ate[num_filosofos];
int intrupt[num_filosofos];//platos

//c) Todo filósofo debe tener un estomago.
void estomago(){
    for(int i=0;i<num_filosofos;i++){
        ate[i]=100;//estomago del folosofo
        intrupt[i]=50;
    }
}

void print(string str){
	cout_mutex.lock();
	cout<<str<<endl;
	cout_mutex.unlock();
}


void piensa(int id){

//d) Cuando un filósofo piensa se demora un tiempo aleatorio(Random), donde decrementa el valor de su estómago por el gasto de energía.

    int thinking=1000+rand()%(2000-1000);//tiempo aleatorio para pensar
    this_thread::sleep_for(chrono::nanoseconds(thinking));
    print("Filosofo " + to_string(id) + " esta pensando...");
    ate[id]-=25;//reduccion del estomago por pensar

//g) Un filosofo se puede morir de hambre cuando su estomago esta cero(o menos de cero) por un determinado tiempo que puede esperar hambriento.
    if(ate[id]<=0 && thinking>=1050){
    print("Filosofo "+ to_string(id) + " murio por hambre");//muerte por hambre
  }
}



bool come(int id, int left, int right){ 

	while(1) if(mtx[left].try_lock()){ //probar levantando el cubierto izquierdo

//f) Cuando de un filósofo se llena su estomago, entonces debe dejar de comer.
	if(ate[id]>100) return true;//verificaciopn si esta lleno
    print("Filosofo "+ to_string(id) + " agarra el cubierto izquierdo "+ to_string(left));

//e) Cuando un fílosofo come se demora un tiempo aleatorio(Random), donde incrementa el valor de su estómago.
	if(mtx[right].try_lock()){ //probar levantando el cubierto derecho
        int eating=1000+rand()%(2000-1000);//tiempo aleatorio para comer
        this_thread::sleep_for(chrono::nanoseconds(eating));
		print("Filosofo "+ to_string(id) +" agarra el cubierto derecho "+ to_string(right) +
		"\nFilosofo "+ to_string(id)+" comiendo...");
        ate[id]+=25;//aumento del stomago en 25
		return true;

	}
    else{
		  mtx[left].unlock();
		  piensa(id);//se deja el cubierto por que el otro filosofo tiene el otro cubierto
		
	  }
	}
	return false;
}

void dejar_Cubiertos(int left, int right){
	mtx[left].unlock();//baja el tenedor izquierdo
	mtx[right].unlock();//baja el tenedor derecho
}

void empezar_Comida(int philID){

	int lIndex= min(philID,(philID+1)%(num_filosofos));
	int rIndex= max(philID,(philID+1)%(num_filosofos));

	while(intrupt[philID]-- > 0){ //limite para la hora de comer
		if(come(philID,lIndex,rIndex)){ //filosofo trantando de comer
			dejar_Cubiertos(lIndex,rIndex); 
			this_thread::sleep_for(chrono::milliseconds(1000));//tiempo para poder comer
		}
	}
}

void comida(){
  //threads creados
	for(int i=0;i<num_filosofos;++i){
		filosofos[i]=thread(empezar_Comida, i);
	}
  //threads creados
	for(int i=0; i<num_filosofos;++i){
		filosofos[i].join();
	}
}

int main(){
	estomago();
	comida();
	for(int i=0; i<num_filosofos;i++){
		cout<<"Filosofo "<<i<<" = "<<ate[i] <<endl;
	}
}
